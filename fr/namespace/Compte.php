<?php

namespace fr\Compte;

class Compte
{
    //attribut
    private ?int $id;
    private ?string $type;
    private ?float $solde;
    private ?string $decouvert;
    private ?string $client;
    private ?int $agence;

    //Construct 
    public function __construct(
        ?int $id,
        ?string $type,
        ?float $solde,
        ?string $decouvert,
        ?string $client,
        ?int $agence
    ) {

        $this-> $id = $id;
        $this-> $type = $type;
        $this-> $solde = $solde;
        $this-> $decouvert = $decouvert;
        $this-> $client = $client;
        $this->$agence  = $agence ;
    }
}
